import boto3, os

###########################################################
#
#       Deploy device Job
#
###########################################################

client = boto3.client('iot')

nameDevice = "sdcc1617Device"

policy = client.create_policy(
     policyName=nameDevice+"MyPolicy",
     policyDocument='{"Version": "2012-10-17", "Statement": [{"Effect": "Allow","Action": "iot:*","Resource": "*"}]}'
)

response = client.create_thing(
      thingName= nameDevice
)
response = client.describe_thing(
     thingName=nameDevice
)
name = response['thingName']
print name
clientId = response['defaultClientId']
print clientId
response = client.describe_endpoint()
host = response['endpointAddress']
print host

response = client.create_keys_and_certificate(
  setAsActive=True
)
dropboxPath = "https://www.dropbox.com/s/1kbfgixhhn431ec/iotJS.tar.gz?dl=0"
os.system("wget " + dropboxPath + " -O iotJS.tar.gz \n tar -zxvf iotJS.tar.gz \n")
os.system("cd iotJS/ \n mkdir ./certs/")
os.system("cd iotJS/certs/\n wget https://www.symantec.com/content/en/us/enterprise/verisign/roots/VeriSign-Class%203-Public-Primary-Certification-Authority-G5.pem -O rootCA.pem")

certificatePem = response['certificatePem']
publicKey = response['keyPair']['PublicKey']
privateKey = response['keyPair']['PrivateKey']
certificateId = response['certificateId']
certificateArn = response['certificateArn']
prefixFile = certificateId[0:10]
fileCertificatePem = open("./iotJS/certs/"+prefixFile+'-certificate.pem.crt', 'w+')
fileCertificatePem.write(certificatePem)
fileCertificatePem.close()

filePublicKey = open("./iotJS/certs/"+prefixFile+'-private.pem.key', 'w+')
filePublicKey.write(privateKey)
filePublicKey.close()
filePrivateKey = open("./iotJS/certs/"+prefixFile+'-public.pem.key', 'w+')
filePrivateKey.write(publicKey)
filePrivateKey.close()

response = client.attach_principal_policy(
    policyName=nameDevice+"MyPolicy",
    principal=certificateArn
)
response = client.attach_thing_principal(
    thingName=name,
    principal=certificateArn
)

#clientId = "raspberrypi"
#name = "mything"
#host = "a1yjgaiunqyy3l.iot.eu-west-1.amazonaws.com"
#prefixFile="9c208c95ec"
#clientId="sdcc1617Device"
#name="sdcc1617Device"
cdn = open("./cdn.txt","r")
cdnName = cdn.readline()
bucket = open("./bucket.txt","r")
bucketName = bucket.readline()
os.system("node iotJS/device.js --keyPath=./iotJS/certs/"+prefixFile+"-private.pem.key --certPath=./iotJS/certs/"+prefixFile+"-certificate.pem.crt --caPath=./iotJS/certs/rootCA.pem --clientId="+clientId +" --name="+name+" --host="+host +" --bucketName="+ bucketName+ " --cdnName=" +cdnName)

