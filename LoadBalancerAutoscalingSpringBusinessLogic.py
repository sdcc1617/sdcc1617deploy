import boto3,shlex, time

###########################################################
#
#       Auto Scaling Settings
#
###########################################################

def confAutoScalingBusinessLogic(regionNameVPC,lbTg,subnets,zonename,sgID, rdsAddress):

                        #
                        #       Read Config Params from "configAutoscaling.txt"

                        #

    # Create IAM client
    client = boto3.client('iam')

    response = client.create_instance_profile(
        InstanceProfileName='adminBLogic',
        Path='/'
    )
    arn = response['InstanceProfile']['Arn']
    response = client.create_role(
        Path='/',
        RoleName='adminBLogic',
        AssumeRolePolicyDocument= '{"Version": "2012-10-17","Statement": [{"Effect": "Allow", "Principal":{"Service":["ec2.amazonaws.com"]}, "Action": "sts:AssumeRole"}]}',
        Description='Admin role for Business logic'
    )

    client.attach_role_policy(
        PolicyArn='arn:aws:iam::aws:policy/AdministratorAccess',
        RoleName='adminBLogic'
    )

    response = client.add_role_to_instance_profile(
        InstanceProfileName='adminBLogic',
        RoleName='adminBLogic'
    )
    time.sleep(30) 

    paramConfigFileAutoscaling  = open(regionNameVPC+"/configAutoscalingBusinessLogic.txt","r")

    launchConfigName            = paramConfigFileAutoscaling.readline().split(",")[1]
    keyname                     = paramConfigFileAutoscaling.readline().split(",")[1]
    asgname                     = paramConfigFileAutoscaling.readline().split(",")[1]
    instanceSize                = paramConfigFileAutoscaling.readline().split(",")[1]
    imageEC2ID                  = paramConfigFileAutoscaling.readline().split(",")[1]
    maxSize                     = int(paramConfigFileAutoscaling.readline().split(",")[1])
    minSize                     = int(paramConfigFileAutoscaling.readline().split(",")[1])
    desiredCapacity             = int(paramConfigFileAutoscaling.readline().split(",")[1])
    defaultCooldown             = int(paramConfigFileAutoscaling.readline().split(",")[1])
    healthCheckGracePeriod      = int(paramConfigFileAutoscaling.readline().split(",")[1])

    launchConfigName            = shlex.split(launchConfigName)[0]
    keyname                     = shlex.split(keyname)[0]
    asgname                     = shlex.split(asgname)[0]
    instanceSize                = shlex.split(instanceSize)[0]
    imageEC2ID                  = shlex.split(imageEC2ID)[0]


    allSubnets = subnets[0]+","+subnets[1]+","+subnets[2]

    autoScaling = boto3.client('autoscaling',region_name=regionNameVPC)
    time.sleep(30) 
                        ####    Launch Configuration
    responseAS = autoScaling.create_launch_configuration(
                            LaunchConfigurationName =  launchConfigName,
                            ImageId = imageEC2ID,
                            KeyName = keyname,
                            SecurityGroups= [sgID],
                            UserData= "#!/bin/bash\nyum update -y\ncd /home/ec2-user\nwget https://www.dropbox.com/s/37wlqfqxr24vl8v/sdcc.business.logic-0.0.1-SNAPSHOT.jar?dl=0 -O app.jar\nsudo yum install -y java-1.8.0-openjdk.x86_64\nsudo /usr/sbin/alternatives --set java /usr/lib/jvm/jre-1.8.0-openjdk.x86_64/bin/java\necho export RDS_HOST=" + rdsAddress +" >> .bashrc\necho export RDS_PORT=3306 >> .bashrc\necho export RDS_USERNAME=fabrizio >> .bashrc\necho export RDS_PASSWORD=fabrizio >> .bashrc\necho export JAVA_HOME=/usr/lib/jvm/jdk-1.8.0-openjdk.x86_64 >> .bashrc\necho export PATH=$JAVA_HOME/bin:$PATH >> .bashrc\nsource .bashrc\njava -jar app.jar",
                            InstanceType= instanceSize,
                            BlockDeviceMappings= [
                                {
                                    'DeviceName': '/dev/xvda',
                                    'Ebs': {
                                        'VolumeType': 'gp2',
                                        'VolumeSize': 8,
                                        'DeleteOnTermination': True
                                            }
                                }],
                            InstanceMonitoring= {'Enabled': True },
                            EbsOptimized= False,
                            IamInstanceProfile=arn,
                            AssociatePublicIpAddress=True
                            )
    print "[AUTOSCALING CONF. NAME] --- ",launchConfigName

                        ####    Create Auto-Scaling Group
    responseASG = autoScaling.create_auto_scaling_group(
                            AutoScalingGroupName= asgname,
                            LaunchConfigurationName= launchConfigName,
                            MaxSize= maxSize,
                            MinSize= minSize,
                            DesiredCapacity= desiredCapacity,
                            DefaultCooldown= defaultCooldown,
                            AvailabilityZones= [zonename[0],zonename[1],zonename[2]],
                            TargetGroupARNs= [lbTg],
                            HealthCheckType= 'EC2',
                            HealthCheckGracePeriod= healthCheckGracePeriod,
                            VPCZoneIdentifier= allSubnets,
                            TerminationPolicies= ['Default'],
                            NewInstancesProtectedFromScaleIn= True
                           )

    print "[AUTOSCALING GROUP NAME] --- ",asgname
    return


###########################################################
#
#       Load Balancer Settings
#
###########################################################

def configParametersBusinessLogic(regionNameVPC,vpcID,subnets,sgID,zonename, rdsAddress):

                        #
                        #       read Config Params from "configLoadBalancer.txt"
                        #
    paramConfigFileLB   = open(regionNameVPC+"/configLoadBalancerBusinessLogic.txt","r")

    nameLB              = paramConfigFileLB.readline().split(",")[1]
    targetGroupName     = paramConfigFileLB.readline().split(",")[1]
    protocol            = paramConfigFileLB.readline().split(",")[1]
    port                = int(paramConfigFileLB.readline().split(",")[1])
    healthCheckTimeSeconds      = int(paramConfigFileLB.readline().split(",")[1])
    healthyThresholdCount       = int(paramConfigFileLB.readline().split(",")[1])
    unhealthyThresholdCount     = int(paramConfigFileLB.readline().split(",")[1])
    healthCheckIntervalSeconds  = int(paramConfigFileLB.readline().split(",")[1])

    nameLB              = shlex.split(nameLB)[0]
    targetGroupName     = shlex.split(targetGroupName)[0]
    protocol            = shlex.split(protocol)[0]

    loadBalancer = boto3.client('elbv2',region_name=regionNameVPC)
    

    responseLB = loadBalancer.create_load_balancer(
                    Name= nameLB,
                    Subnets= [subnets[0], subnets[1], subnets[2]],
                    SecurityGroups= [sgID],
                    Scheme= 'internet-facing',
                    Type= 'application', 
                    )

    lbArn = responseLB['LoadBalancers'][0]['LoadBalancerArn']
    elb = responseLB['LoadBalancers'][0]['DNSName']

                        ####    Target Group
    responseTG = loadBalancer.create_target_group(
                    Name = targetGroupName,
                    Protocol= protocol,
                    Port= port,
                    VpcId= vpcID,
                    HealthCheckPort= 'traffic-port',
                    HealthCheckTimeoutSeconds= healthCheckTimeSeconds,
                    HealthCheckProtocol= protocol,
                    HealthyThresholdCount=healthyThresholdCount,
                    UnhealthyThresholdCount= unhealthyThresholdCount,
                    HealthCheckPath= '/',
                    HealthCheckIntervalSeconds= healthCheckIntervalSeconds,
                    Matcher= {  'HttpCode': '200' },
                )

    lbTg = responseTG['TargetGroups'][0]['TargetGroupArn']


                    ### Attach a Listener: Assign the port number 8000
    responseLB = loadBalancer.create_listener(
                    LoadBalancerArn = lbArn,
                    Protocol=protocol,
                    Port=port,
                    DefaultActions=[
                    {
                        'Type':'forward',
                        'TargetGroupArn': lbTg
                    },])
    responseLB = loadBalancer.create_listener(
                    LoadBalancerArn = lbArn,
                    Protocol=protocol,
                    Port=80,
                    DefaultActions=[
                    {
                        'Type':'forward',
                        'TargetGroupArn': lbTg
                    },])

    print "\n[LOAD BALANCER] ------------ ",nameLB
    print "[TARGET GROUP] ------------- ",targetGroupName
    
    confAutoScalingBusinessLogic(regionNameVPC,lbTg,subnets,zonename,sgID, rdsAddress)

    return elb