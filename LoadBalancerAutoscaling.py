import boto3,shlex

###########################################################
#
#       Auto Scaling Settings
#
###########################################################

def confAutoScaling(regionNameVPC,lbTg,subnets,zonename,sgID):

                        #
                        #       Read Config Params from "configAutoscaling.txt"
                        #
    paramConfigFileAutoscaling  = open(regionNameVPC+"/configAutoscaling.txt","r")

    launchConfigName            = paramConfigFileAutoscaling.readline().split(",")[1]
    keyname                     = paramConfigFileAutoscaling.readline().split(",")[1]
    asgname                     = paramConfigFileAutoscaling.readline().split(",")[1]
    instanceSize                = paramConfigFileAutoscaling.readline().split(",")[1]
    imageEC2ID                  = paramConfigFileAutoscaling.readline().split(",")[1]


    launchConfigName            = shlex.split(launchConfigName)[0]
    keyname                     = shlex.split(keyname)[0]
    asgname                     = shlex.split(asgname)[0]
    instanceSize                = shlex.split(instanceSize)[0]
    imageEC2ID                  = shlex.split(imageEC2ID)[0]

    allSubnets = subnets[0]+","+subnets[1]+","+subnets[2]

    autoScaling = boto3.client('autoscaling',region_name=regionNameVPC)
    
                        ####    Launch Configuration
    responseAS = autoScaling.create_launch_configuration(
                            LaunchConfigurationName =  launchConfigName,
                            ImageId = imageEC2ID,
                            KeyName = keyname,
                            SecurityGroups= [sgID],
                            UserData='#!/bin/bash \n yum update -y \n cd /home/ec2-user \n mkdir sdcc \n cd sdcc \n sudo yum -y install go \n wget https://s3.amazonaws.com/davnan-sdcc/server.go \n go build \n ./sdcc',      
                            InstanceType= instanceSize,
                            BlockDeviceMappings= [
                                {
                                    'DeviceName': '/dev/xvda',
                                    'Ebs': {
                                        'SnapshotId': 'snap-0ffa67ca805adfd5f',
                                        'VolumeType': 'gp2',
                                        'VolumeSize': 8,
                                        'DeleteOnTermination': True
                                            }
                                }],
                            InstanceMonitoring= {'Enabled': True },
                            EbsOptimized= False
                            )
    print "[AUTOSCALING CONF. NAME] --- ",launchConfigName

                        ####    Create Auto-Scaling Group
    responseASG = autoScaling.create_auto_scaling_group(
                            AutoScalingGroupName= asgname,
                            LaunchConfigurationName= launchConfigName,
                            MaxSize= 1,
                            MinSize= 1,
                            DesiredCapacity= 1,
                            DefaultCooldown= 300,
                            AvailabilityZones= [zonename[0],zonename[1],zonename[2]],
                            TargetGroupARNs= [lbTg],
                            HealthCheckType= 'EC2',
                            HealthCheckGracePeriod= 300,
                            VPCZoneIdentifier= allSubnets,
                            TerminationPolicies= ['Default'],
                            NewInstancesProtectedFromScaleIn= True
                           )

    print "[AUTOSCALING GROUP NAME] --- ",asgname
    return


###########################################################
#
#       Load Balancer Settings
#
###########################################################

def configParameters(regionNameVPC,vpcID,subnets,sgID,zonename):

                        #
                        #       read Config Params from "configLoadBalancer.txt"
                        #
    paramConfigFileLB   = open(regionNameVPC+"/configLoadBalancer.txt","r")

    nameLB              = paramConfigFileLB.readline().split(",")[1]
    targetGroupName     = paramConfigFileLB.readline().split(",")[1]
    protocol            = paramConfigFileLB.readline().split(",")[1]
    port                = int(paramConfigFileLB.readline().split(",")[1])

    nameLB              = shlex.split(nameLB)[0]
    targetGroupName     = shlex.split(targetGroupName)[0]
    protocol            = shlex.split(protocol)[0]


    loadBalancer = boto3.client('elbv2',region_name=regionNameVPC)
    

    responseLB = loadBalancer.create_load_balancer(
                    Name= nameLB,
                    Subnets= [subnets[0], subnets[1], subnets[2]],
                    SecurityGroups= [sgID],
                    Scheme= 'internet-facing',
                    Type= 'application', 
                    )

    lbArn = responseLB['LoadBalancers'][0]['LoadBalancerArn']


                        ####    Target Group
    responseTG = loadBalancer.create_target_group(
                    Name = targetGroupName,
                    Protocol= protocol,
                    Port= port,
                    VpcId= vpcID,
                    HealthCheckPort= 'traffic-port',
                    HealthCheckTimeoutSeconds= 5,
                    HealthCheckProtocol= protocol,
                    HealthyThresholdCount= 5,
                    UnhealthyThresholdCount= 2,
                    HealthCheckPath= '/',
                    HealthCheckIntervalSeconds= 30,
                    Matcher= {  'HttpCode': '200' },
                )

    lbTg = responseTG['TargetGroups'][0]['TargetGroupArn']


                    ### Attach a Listener: Assign the port number 8000
    responseLB = loadBalancer.create_listener(
                    LoadBalancerArn = lbArn,
                    Protocol=protocol,
                    Port=port,
                    DefaultActions=[
                    {
                        'Type':'forward',
                        'TargetGroupArn': lbTg
                    },])

    print "\n[LOAD BALANCER] ------------ ",nameLB
    print "[TARGET GROUP] ------------- ",targetGroupName
    
    confAutoScaling(regionNameVPC,lbTg,subnets,zonename,sgID)

    return

