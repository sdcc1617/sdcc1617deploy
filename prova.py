import boto3, json, shlex, os, getpass

client = boto3.client('autoscaling')
response = client.put_scaling_policy(
	AutoScalingGroupName= "rrrrrrrrr",
    PolicyName='DecreaseGroupSize',
    PolicyType='StepScaling',
    AdjustmentType='ChangeInCapacity',
    StepAdjustments=[
        {
            'ScalingAdjustment': -1,
            'MetricIntervalUpperBound': 0.0
        }
    ],
    EstimatedInstanceWarmup=60,
    Cooldown=60,
)

policyARN = response['PolicyARN']
print policyARN

client = boto3.client('cloudwatch')
response = client.put_metric_alarm(
	AlarmName='scale-in-alarm',
    AlarmDescription='Allarme per fare scale in',
    ActionsEnabled=True,
    OKActions=[],
    AlarmActions=[
        policyARN,
    ],
    InsufficientDataActions=[],
    MetricName='NetworkIn',
    Namespace='AWS/EC2',
    Statistic='Average',
    #ExtendedStatistic='string',
    Dimensions=[
        {
            'Name': 'AutoScalingGroupName',
            'Value': 'rrrrrrrrr' #nome dell'autoscaling group
        },
    ],
    Period=60,
    EvaluationPeriods=1,
    #DatapointsToAlarm=123,
    Threshold=100000.0,
    ComparisonOperator='LessThanThreshold',
    #TreatMissingData='string',
    #EvaluateLowSampleCountPercentile='string'
)

client = boto3.client('autoscaling')
response = client.put_scaling_policy(
	AutoScalingGroupName= "rrrrrrrrr",
    PolicyName='IncreaseGroupSize',
    PolicyType='StepScaling',
    AdjustmentType='ChangeInCapacity',
    StepAdjustments=[
        {
            'ScalingAdjustment': 1,
            'MetricIntervalUpperBound': 0.0
        }
    ],
    EstimatedInstanceWarmup=60,
    Cooldown=60,
)

policyARN = response['PolicyARN']
print policyARN

client = boto3.client('cloudwatch')
response = client.put_metric_alarm(
	AlarmName='scale-out-alarm',
    AlarmDescription='Allarme per fare scale out',
    ActionsEnabled=True,
    OKActions=[],
    AlarmActions=[
        policyARN,
    ],
    InsufficientDataActions=[],
    MetricName='NetworkIn',
    Namespace='AWS/EC2',
    Statistic='Average',
    #ExtendedStatistic='string',
    Dimensions=[
        {
            'Name': 'AutoScalingGroupName',
            'Value': 'rrrrrrrrr' #nome dell'autoscaling group
        },
    ],
    Period=60,
    EvaluationPeriods=1,
    #DatapointsToAlarm=123,
    Threshold=100000.0,
    ComparisonOperator='GreaterThanOrEqualToThreshold',
    #TreatMissingData='string',
    #EvaluateLowSampleCountPercentile='string'
)