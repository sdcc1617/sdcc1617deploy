import boto3
from botocore.exceptions import ClientError
import time
###########################################################
#
#       Security Group
#
###########################################################

def configParameters(ec2,vpcID):
    try:
                            #       create a security group
        sg = ec2.create_security_group( Description='Security Group for SDCC1617 Project',
                                        GroupName='sdcc1617project',
                                        VpcId=vpcID )

        sgAuthIngress = sg.authorize_ingress(IpProtocol="-1",CidrIp="0.0.0.0/0",FromPort=80,ToPort=80)

    
    except ClientError as e:
        print(e)
    
    print "\n[SECURITY GROUP ID] -------- ",sg.id

    return sg.id

def configParametersEtcd(ec2,vpcID):
    try:
		            #       create a security group
        sg = ec2.create_security_group( Description='Security Group for SDCC1617 Project, Etcd Cluster',
		                              GroupName='sdcc1617projectEtcd',
		                              VpcId=vpcID )

        sgAuthIngress = sg.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=1,ToPort=65535)
    
    except ClientError as e:
        print(e)
    
    print "\n[SECURITY GROUP ID] -------- ",sg.id

    return sg.id

def configParametersGoService(ec2,vpcID):
    try:
                    #       create a security group
        sg = ec2.create_security_group( Description='Security Group for SDCC1617 Project, Go Service',
                                      GroupName='sdcc1617projectGoService',
                                      VpcId=vpcID )

        sgAuthIngress = sg.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=1,ToPort=65535)
    
    except ClientError as e:
        print(e)
    
    print "\n[SECURITY GROUP ID] -------- ",sg.id

    return sg.id

def configParametersSpringBusinessLogic(ec2,vpcID):
    try:
                    #       create a security group
        sg = ec2.create_security_group( Description='Security Group for SDCC1617 Project, Spring Business Logic',
                                      GroupName='sdcc1617projectSpringBusinessLogic',
                                      VpcId=vpcID )

        sgAuthIngress = sg.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=1,ToPort=65535)
    
    except ClientError as e:
        print(e)
    
    print "\n[SECURITY GROUP ID] -------- ",sg.id

    return sg.id


def configParametersRDS(ec2,vpcID):
    try:
                    #       create a security group
        sg = ec2.create_security_group( Description='Security Group for SDCC1617 Project, RDS',
                                      GroupName='sdcc1617projectRDS',
                                      VpcId=vpcID )

        sgAuthIngress = sg.authorize_ingress(IpProtocol="tcp",CidrIp="0.0.0.0/0",FromPort=1,ToPort=65535)
    
    except ClientError as e:
        print(e)
    time.sleep(5)
    print "\n[SECURITY GROUP ID] -------- ",sg.id

    return sg.id    
