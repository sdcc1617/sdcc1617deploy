import boto3,shlex, time, getpass

###########################################################
#
#       Auto Scaling Settings
#
###########################################################

def confAutoScalingGolangService(regionNameVPC,lbTg,subnets,zonename,sgID, elbEtcd, cloudfrontName, dynamoDBTables, bucketname):
    #
    # Read Config Params from "configAutoscaling.txt"

    #
    # Create IAM client
    client = boto3.client('iam')

    response = client.create_instance_profile(
        InstanceProfileName='adminGoService',
        Path='/'
    )
    arn = response['InstanceProfile']['Arn']
    response = client.create_role(
        Path='/',
        RoleName='adminGoService',
        AssumeRolePolicyDocument= '{"Version": "2012-10-17","Statement": [{"Effect": "Allow", "Principal":{"Service":["ec2.amazonaws.com"]}, "Action": "sts:AssumeRole"}]}',
        Description='Admin role for Go Service'
    )

    client.attach_role_policy(
        PolicyArn='arn:aws:iam::aws:policy/AdministratorAccess',
        RoleName='adminGoService'
    )

    response = client.add_role_to_instance_profile(
        InstanceProfileName='adminGoService',
        RoleName='adminGoService'
    )
    time.sleep(30) 

    paramConfigFileAutoscaling  = open(regionNameVPC+"/configAutoscalingGolangService.txt","r")

    launchConfigName            = paramConfigFileAutoscaling.readline().split(",")[1]
    keyname                     = paramConfigFileAutoscaling.readline().split(",")[1]
    asgname                     = paramConfigFileAutoscaling.readline().split(",")[1]
    instanceSize                = paramConfigFileAutoscaling.readline().split(",")[1]
    imageEC2ID                  = paramConfigFileAutoscaling.readline().split(",")[1]
    maxSize                     = int(paramConfigFileAutoscaling.readline().split(",")[1])
    minSize                     = int(paramConfigFileAutoscaling.readline().split(",")[1])
    desiredCapacity             = int(paramConfigFileAutoscaling.readline().split(",")[1])
    defaultCooldown             = int(paramConfigFileAutoscaling.readline().split(",")[1])
    healthCheckGracePeriod      = int(paramConfigFileAutoscaling.readline().split(",")[1])
    estimatedInstanceWarmupScaleIn  = int(paramConfigFileAutoscaling.readline().split(",")[1])
    cooldownScaleIn             = int(paramConfigFileAutoscaling.readline().split(",")[1])
    evaluationPeriodsScaleIn    = int(paramConfigFileAutoscaling.readline().split(",")[1])
    thresholdScaleIn            = float(paramConfigFileAutoscaling.readline().split(",")[1])
    estimatedInstanceWarmupScaleOut  = int(paramConfigFileAutoscaling.readline().split(",")[1])
    cooldownScaleOut            = int(paramConfigFileAutoscaling.readline().split(",")[1])
    evaluationPeriodsScaleOut   = int(paramConfigFileAutoscaling.readline().split(",")[1])
    thresholdScaleOut           = float(paramConfigFileAutoscaling.readline().split(",")[1])
    scalingAdjustmentIn         = int(paramConfigFileAutoscaling.readline().split(",")[1])
    scalingAdjustmentOut        = int(paramConfigFileAutoscaling.readline().split(",")[1])

    launchConfigName                = shlex.split(launchConfigName)[0]
    keyname                         = shlex.split(keyname)[0]
    asgname                         = shlex.split(asgname)[0]
    instanceSize                    = shlex.split(instanceSize)[0]
    imageEC2ID                      = shlex.split(imageEC2ID)[0]


    allSubnets = subnets[0]+","+subnets[1]+","+subnets[2]

    autoScaling = boto3.client('autoscaling',region_name=regionNameVPC)

                        ####    Launch Configuration
    directory = "dir"#"unassigned"

    dropboxPath = "https://www.dropbox.com/s/68dja4x8oi8k17g/Server.tar.gz?dl=0"
    #Va inserita una istruzione per creare una la cartella .aws con il file credential e config

    ###############################################################################
    #
    # Prelevo credenziali e li salvo sul launch config
    #
    ###############################################################################

    lines = [line.rstrip('\n') for line in open("/home/"+getpass.getuser()+"/.aws/config")]

    strLineConfig = ""
    for line in lines:
        strLineConfig = strLineConfig + line + "\n"

    #print strLineConfig

    lines = [line.rstrip('\n') for line in open("/home/"+getpass.getuser()+"/.aws/credentials")]

    strLineCredentials = ""
    for line in lines:
        strLineCredentials = strLineCredentials + line + "\n"

    #print strLineCredentials

    fileConfig = "sudo mkdir .aws\n cd .aws\n sudo touch config\n sudo touch credentials\nsudo echo "+ "\""+ strLineConfig + "\"" + " >> config \n"+ "echo "+ "\""+ strLineCredentials +"\""+ " >> credentials\n\ncd ..\n"
    UserData= "#!/bin/bash \n yum update -y \n cd /home/ec2-user \n"+ fileConfig + "mkdir sdcc \n cd sdcc \n sudo yum -y install go \nwget "+ dropboxPath+" -O Server.tar.gz \n tar -zxvf Server.tar.gz \n cd Server/ \n ./Server "+elbEtcd +" "+cloudfrontName+ " "+ regionNameVPC + " " + bucketname+"/" + " " + dynamoDBTables[1] + " "+ dynamoDBTables[2] + " " + directory
    print UserData
    responseAS = autoScaling.create_launch_configuration(
                            LaunchConfigurationName =  launchConfigName,
                            ImageId = imageEC2ID,
                            KeyName = keyname,
                            SecurityGroups= [sgID],                                                                             #\n sudo yum -y install go 
                            UserData= "#!/bin/bash \n yum update -y \n cd /home/ec2-user\necho export AWS_REGION=eu-west-1 >> .bashrc\nsource .bashrc\n"+ fileConfig +"mkdir sdcc \n cd sdcc \nwget "+ dropboxPath+" -O Server.tar.gz \n tar -zxvf Server.tar.gz \n cd Server/ \n ./Server "+elbEtcd +" "+cloudfrontName+ " "+ regionNameVPC + " " + bucketname+"/" + " " + dynamoDBTables[1] + " "+ dynamoDBTables[2] + " " + directory,           
                            InstanceType= instanceSize,
                            BlockDeviceMappings= [
                                {
                                    'DeviceName': '/dev/xvda',
                                    'Ebs': {
                                        'VolumeType': 'gp2',
                                        'VolumeSize': 8,
                                        'DeleteOnTermination': True
                                            }
                                }],
                            InstanceMonitoring= {'Enabled': True },
                            EbsOptimized= False,
                            IamInstanceProfile=arn,
                            AssociatePublicIpAddress=True
                            )
    print "[AUTOSCALING CONF. NAME] --- ",launchConfigName

                        ####    Create Auto-Scaling Group
    responseASG = autoScaling.create_auto_scaling_group(
                            AutoScalingGroupName= asgname,
                            LaunchConfigurationName= launchConfigName,
                            MaxSize= maxSize, #9
                            MinSize= minSize,
                            DesiredCapacity= desiredCapacity,
                            DefaultCooldown= defaultCooldown, #60 secondi, consigliati 180 (e' il periodo in cui viene sospeso lo scaling)
                            AvailabilityZones= [zonename[0],zonename[1],zonename[2]],
                            TargetGroupARNs= [lbTg],
                            HealthCheckType= 'ELB', 
                            HealthCheckGracePeriod= healthCheckGracePeriod, #60 secondi
                            VPCZoneIdentifier= allSubnets,
                            TerminationPolicies= ['Default'], #approfondire
                            #NewInstancesProtectedFromScaleIn= True
                           )

    ###########################################################
    #
    #       Creazione scaling policy e allarme cloudwatch
    #
    ###########################################################
    client = boto3.client('autoscaling')
    response = client.put_scaling_policy(
        AutoScalingGroupName= asgname,
        PolicyName='DecreaseGroupSize',
        PolicyType='StepScaling',
        AdjustmentType='ChangeInCapacity',
        StepAdjustments=[
            {
                'ScalingAdjustment': scalingAdjustmentIn,
                'MetricIntervalUpperBound': 0.0
            }
        ],
        EstimatedInstanceWarmup=estimatedInstanceWarmupScaleIn,
        Cooldown=cooldownScaleIn,
    )

    policyARN = response['PolicyARN']
    print policyARN

    client = boto3.client('cloudwatch')
    response = client.put_metric_alarm(
        AlarmName='scale-in-alarm',
        AlarmDescription='Allarme per fare scale in',
        ActionsEnabled=True,
        OKActions=[],
        AlarmActions=[
            policyARN,
        ],
        InsufficientDataActions=[],
        MetricName='NetworkIn',
        Namespace='AWS/EC2',
        Statistic='Maximum',
        #ExtendedStatistic='string',
        Dimensions=[
            {
                'Name': 'AutoScalingGroupName',
                'Value': asgname #nome dell'autoscaling group
            },
        ],
        Period=60,
        EvaluationPeriods=evaluationPeriodsScaleIn,
        #DatapointsToAlarm=123,
        Threshold=thresholdScaleIn,
        ComparisonOperator='LessThanThreshold',
        #TreatMissingData='string',
        #EvaluateLowSampleCountPercentile='string'
    )

    client = boto3.client('autoscaling')
    response = client.put_scaling_policy(
        AutoScalingGroupName= asgname,
        PolicyName='IncreaseGroupSize',
        PolicyType='StepScaling',
        AdjustmentType='ChangeInCapacity',
        StepAdjustments=[
            {
                'ScalingAdjustment': scalingAdjustmentOut,
                'MetricIntervalUpperBound': 0.0
            }
        ],
        EstimatedInstanceWarmup=estimatedInstanceWarmupScaleOut,
        Cooldown=cooldownScaleOut,
    )

    policyARN = response['PolicyARN']
    print policyARN

    client = boto3.client('cloudwatch')
    response = client.put_metric_alarm(
        AlarmName='scale-out-alarm',
        AlarmDescription='Allarme per fare scale out',
        ActionsEnabled=True,
        OKActions=[],
        AlarmActions=[
            policyARN,
        ],
        InsufficientDataActions=[],
        MetricName='NetworkIn',
        Namespace='AWS/EC2',
        Statistic='Maximum',
        #ExtendedStatistic='string',
        Dimensions=[
            {
                'Name': 'AutoScalingGroupName',
                'Value': asgname #nome dell'autoscaling group
            },
        ],
        Period=60,
        EvaluationPeriods=evaluationPeriodsScaleOut,
        #DatapointsToAlarm=123,
        Threshold=thresholdScaleOut,
        ComparisonOperator='GreaterThanOrEqualToThreshold',
        #TreatMissingData='string',
        #EvaluateLowSampleCountPercentile='string'
    )
    print "[AUTOSCALING GROUP NAME] --- ",asgname
    return

###########################################################
#
#       Load Balancer Settings
#
###########################################################

def configParametersGolangService(regionNameVPC,vpcID,subnets,sgID,zonename, elbEtcd, cloudfrontName, dynamoDBTables, bucketname):

                        #
                        #       read Config Params from "configLoadBalancer.txt"
                        #
    paramConfigFileLB   = open(regionNameVPC+"/configLoadBalancerGolangService.txt","r")

    nameLB              = paramConfigFileLB.readline().split(",")[1]
    targetGroupName     = paramConfigFileLB.readline().split(",")[1]
    protocol            = paramConfigFileLB.readline().split(",")[1]
    port                = int(paramConfigFileLB.readline().split(",")[1])
    healthCheckTimeSeconds      = int(paramConfigFileLB.readline().split(",")[1])
    healthyThresholdCount       = int(paramConfigFileLB.readline().split(",")[1])
    unhealthyThresholdCount     = int(paramConfigFileLB.readline().split(",")[1])
    healthCheckIntervalSeconds  = int(paramConfigFileLB.readline().split(",")[1])

    nameLB              = shlex.split(nameLB)[0]
    targetGroupName     = shlex.split(targetGroupName)[0]
    protocol            = shlex.split(protocol)[0]

    loadBalancer = boto3.client('elbv2',region_name=regionNameVPC)
    

    responseLB = loadBalancer.create_load_balancer(
                    Name= nameLB,
                    Subnets= [subnets[0], subnets[1], subnets[2]],
                    SecurityGroups= [sgID],
                    Scheme= 'internet-facing',
                    Type= 'application', 
                    )

    lbArn = responseLB['LoadBalancers'][0]['LoadBalancerArn']
    elb = responseLB['LoadBalancers'][0]['DNSName']

                        ####    Target Group
    responseTG = loadBalancer.create_target_group(
                    Name = targetGroupName,
                    Protocol= protocol,
                    Port= port,
                    VpcId= vpcID,
                    HealthCheckPort= 'traffic-port',
                    HealthCheckTimeoutSeconds= healthCheckTimeSeconds,
                    HealthCheckProtocol= protocol,
                    HealthyThresholdCount= healthyThresholdCount,
                    UnhealthyThresholdCount= unhealthyThresholdCount,
                    HealthCheckPath= '/getEtcdMembers',
                    HealthCheckIntervalSeconds= healthCheckIntervalSeconds,
                    Matcher= {  'HttpCode': '200' },
                )

    lbTg = responseTG['TargetGroups'][0]['TargetGroupArn']


                    ### Attach a Listener: Assign the port number 8000
    responseLB = loadBalancer.create_listener(
                    LoadBalancerArn = lbArn,
                    Protocol=protocol,
                    Port=port,
                    DefaultActions=[
                    {
                        'Type':'forward',
                        'TargetGroupArn': lbTg
                    },])
    responseLB = loadBalancer.create_listener(
                    LoadBalancerArn = lbArn,
                    Protocol=protocol,
                    Port=80,
                    DefaultActions=[
                    {
                        'Type':'forward',
                        'TargetGroupArn': lbTg
                    },])

    print "\n[LOAD BALANCER] ------------ ",nameLB
    print "[TARGET GROUP] ------------- ",targetGroupName
    
    confAutoScalingGolangService(regionNameVPC,lbTg,subnets,zonename,sgID, elbEtcd, cloudfrontName, dynamoDBTables, bucketname)

    return elb
