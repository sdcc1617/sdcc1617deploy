###########################################################
#
#       S3 Bucket Settings
#
###########################################################

def config():
                        #
                        #       Read Config Params from "configS3Bucket.txt"
                        #
    paramsConfigFiles3  = open("configS3Bucket.txt","r")

    bucketname          = paramsConfigFiles3.readline().split(",")[1]
    acl                 = paramsConfigFiles3.readline().split(",")[1]
    path                = paramsConfigFiles3.readline().split(",")[1]
    objectExec          = paramsConfigFiles3.readline().split(",")[1] 
    aclObject           = paramsConfigFiles3.readline().split(",")[1]

    name = bucketname
    
    bucketname  = shlex.split(bucketname)[0]
    acl         = shlex.split(acl)[0]
    path        = shlex.split(path)[0]
    objectExec  = shlex.split(objectExec)[0]
    aclObject   = shlex.split(aclObject)[0]

    
    
                        #       Create the S3 Bucket
                        #
    s3Bucket = boto3.client('s3')

    responseS3 = s3Bucket.create_bucket(
        ACL = acl,
        Bucket=bucketname,
        #CreateBucketConfiguration={ 'LocationConstraint': 'eu-west-1' }
    )

    s3 = boto3.resource('s3')

                        #
                        #    set public permission 
                        #
    s3.Object(bucketname, objectExec).put(Body=open(path, 'rb'))
    obj = s3.Bucket(bucketname).Object(objectExec)
    obj.Acl().put(ACL=aclObject)

    print '[CREATED S3 BUCKET]'
    return name
