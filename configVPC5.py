import boto3,os, shlex
import S3Bucket,SecurityGroup,DynamoDB,LoadBalancerAutoscaling, LoadBalancerAutoscalingEtcdCluster, LoadBalancerAutoscalingGolangService
import LoadBalancerAutoscalingSpringBusinessLogic
from RdsDeploy import createRDS, createSubnet, getEndPointRDS
from DynamoDB import configParametersDynamoDBTables
from S3Bucket import configParametersS3Bucket, cloudFrontS3Bucket
from DeployLambdaAndRules import deployLambdaAndRules
from DeployFrontend import deployFrontend

###########################################################
#
#       Amazon Region Info
#
###########################################################
#
#   Global Services
#
#   for each amazon's regions set all services
#
###########################################################
#
# Codice per la creazione del bucket s3 e Cloudfront
#
###########################################################

bucketname = configParametersS3Bucket()
print bucketname

cloudfrontName = cloudFrontS3Bucket()
#os.system("echo "+ cloudfrontName+ " >> ./iotDeploy/cdn.txt")
#os.system("echo "+ bucketname+ " >> ./iotDeploy/bucket.txt")

print "\n[Bucket S3 e Cloudfront] ------------ ", cloudfrontName

# Al momento c'e' il supporto solo ad una regione (per piu' regioni dovrebbe essere gestita la scalabilita' geografica
# delle tabelle di dynamoDB e dello stato del cluster ETCD)

for regionNameVPC in os.listdir('.'):
  
    if os.path.isdir(regionNameVPC) and (regionNameVPC[0]!="." and regionNameVPC!="lambda" and 
        regionNameVPC!="frontend" and regionNameVPC!="iotDeploy"):
        print regionNameVPC            
        zonename = []
        subnets = []

        ec2 = boto3.resource("ec2",region_name=regionNameVPC)
    
                #   read the owner's subnets and the zonenames
        for vpc in ec2.vpcs.all():
            for az in ec2.meta.client.describe_availability_zones()["AvailabilityZones"]:
                for subnet in vpc.subnets.filter(Filters=[{"Name": "availabilityZone", "Values": [az["ZoneName"]]}]):
                    zonename.append(az["ZoneName"])
                    subnets.append(subnet.id)
    
        vpcID = vpc.id

        print "\n[AMAZON VPC] --------------- ",vpcID
        print "[AMAZON REGION] ------------ ",regionNameVPC 


        configClusters = open(regionNameVPC+"/clustersConfig.txt","r")
        clusters = configClusters.readline().split(",")


        i = 0
        nameClusters = {}
        desideratedAZ = {}

        for cluster in clusters:
            if i == 0:
                print ""
                i = i + 1
                continue    
        
            clusterName = shlex.split(cluster)[0]
        
            clusterAttributes = clusterName.split(":")
            nameClusters[i-1] = clusterAttributes[0]
            desideratedAZ[i-1] = clusterAttributes[1]
            i = i + 1
        print nameClusters
        print desideratedAZ

        ###########################################################
        #
        #       Codice per la creazione delle tabelle
        #
        ###########################################################

        dynamoDBTables = configParametersDynamoDBTables(regionNameVPC)
        print "\n[TABLES] ------------ "
      
        ###########################################################
        #
        #       Codice per settare il Cluster Etcd
        #
        ###########################################################
        numAvailabilityZoneDesiderated = desideratedAZ[0]

        if numAvailabilityZoneDesiderated > len(zonename):
           numAvailabilityZone = len(zonename)
        else:
           numAvailabilityZone = numAvailabilityZoneDesiderated 

        print subnets
        print zonename

        client = boto3.client('ec2')
        zonenameDesiderated = []
        subnetsAlreadyInUse = []
        for zname in client.describe_availability_zones()['AvailabilityZones']:
            zonenameDesiderated.append(zname['ZoneName'])
        subnetsMustToCreate = []
        for zname in zonenameDesiderated:
            i = 0
            for az in zonename:
                if(zname == az):
                    continue
                i= i + 1
            if(i == len(zonename)):
                subnetsMustToCreate.append(zname)

        print zonenameDesiderated
        print subnetsMustToCreate
        
        
        considerated = []
        i = 0
        for az in zonename:
            isConsiderated = False
            for c in considerated:
                if(az == c):
                    isConsiderated = True
            if isConsiderated==False:
                subnetsAlreadyInUse.append(subnets[i])
                considerated.append(az)
            i = i +1

        print subnetsAlreadyInUse
        print considerated

        cidrBlockAlreadyInUse = []
        for ipv4CidrBlock in client.describe_subnets()['Subnets']:
            cidrBlockAlreadyInUse.append(ipv4CidrBlock['CidrBlock'])

        print cidrBlockAlreadyInUse

        i = 2
        cidrBlock = '10.0.1.0/24'
        for az in subnetsMustToCreate:  
            restart = True
            while restart:
                restart = False
                for ipv4CidrBlock in cidrBlockAlreadyInUse:
                    if(ipv4CidrBlock == cidrBlock):
                        cidrBlock = '10.0.'+str(i) +'.0/24'
                        i = i +1
                        restart = True
                        break
            print cidrBlock
            cidrBlockAlreadyInUse.append(cidrBlock)

            subnet = vpc.create_subnet(
                  AvailabilityZone=az,
                  CidrBlock=cidrBlock
            );
            subnet.id
            subnetsAlreadyInUse.append(subnet.id)

        print subnetsAlreadyInUse
        subnets = subnetsAlreadyInUse
        zonename = zonenameDesiderated
        sgID = SecurityGroup.configParametersEtcd(ec2,vpcID)

        #MANCA UN AUTOSCALING FATTO BENE!!!!!!!!
        elbEtcd = LoadBalancerAutoscalingEtcdCluster.configParametersEtcdCluster(regionNameVPC,vpcID,subnets,sgID,zonename)
        print "\n[ELBv2] ------------ ",elbEtcd
        ###########################################################
        #
        #       Codice per Settare il servizio Go
        #
        ###########################################################
        # elbEtcd = "127.0.0.1"
        numAvailabilityZoneDesiderated = desideratedAZ[1]

        if numAvailabilityZoneDesiderated > len(zonename):
           numAvailabilityZone = len(zonename)
        else:
           numAvailabilityZone = numAvailabilityZoneDesiderated 

        print subnets
        print zonename

        client = boto3.client('ec2')
        zonenameDesiderated = []
        subnetsAlreadyInUse = []
        for zname in client.describe_availability_zones()['AvailabilityZones']:
            zonenameDesiderated.append(zname['ZoneName'])
        subnetsMustToCreate = []
        for zname in zonenameDesiderated:
            i = 0
            for az in zonename:
                if(zname == az):
                    continue
                i= i + 1
            if(i == len(zonename)):
                subnetsMustToCreate.append(zname)

        print zonenameDesiderated
        print subnetsMustToCreate
        
        
        considerated = []
        i = 0
        for az in zonename:
            isConsiderated = False
            for c in considerated:
                if(az == c):
                    isConsiderated = True
            if isConsiderated==False:
                subnetsAlreadyInUse.append(subnets[i])
                considerated.append(az)
            i = i +1

        print subnetsAlreadyInUse
        print considerated

        cidrBlockAlreadyInUse = []
        for ipv4CidrBlock in client.describe_subnets()['Subnets']:
            cidrBlockAlreadyInUse.append(ipv4CidrBlock['CidrBlock'])

        print cidrBlockAlreadyInUse

        i = 2
        cidrBlock = '10.0.1.0/24'
        for az in subnetsMustToCreate:  
            restart = True
            while restart:
                restart = False
                for ipv4CidrBlock in cidrBlockAlreadyInUse:
                    if(ipv4CidrBlock == cidrBlock):
                        cidrBlock = '10.0.'+str(i) +'.0/24'
                        i = i +1
                        restart = True
                        break
            print cidrBlock
            cidrBlockAlreadyInUse.append(cidrBlock)

            subnet = vpc.create_subnet(
                  AvailabilityZone=az,
                  CidrBlock=cidrBlock
            );
            subnet.id
            subnetsAlreadyInUse.append(subnet.id)

        print subnetsAlreadyInUse
        subnets = subnetsAlreadyInUse
        zonename = zonenameDesiderated
        sgID = SecurityGroup.configParametersGoService(ec2,vpcID)

        #MANCA UN AUTOSCALING FATTO BENE!!!!!!!!
        elbGO = LoadBalancerAutoscalingGolangService.configParametersGolangService(regionNameVPC,vpcID,subnets,sgID,zonename, elbEtcd, cloudfrontName, dynamoDBTables, bucketname)
        print "\n[ELBv2] ------------ ",elbGO
        
        ###########################################################
        #
        #       Codice per Settare la parte IoT
        #
        ###########################################################
       # deployLambdaAndRules(regionNameVPC)
       # print "\n[LambdaAndRules] ------------ "
        ###########################################################
        #
        #       Codice per la creazione del cluster RDS
        #
        ###########################################################
        
        sgID = SecurityGroup.configParametersRDS(ec2,vpcID)
        createRDS(sgID,vpcID)
        rdsAddress = getEndPointRDS()
        print "\n[RDS] ------------ ",rdsAddress
        ###########################################################
        #
        #       Codice per il settaggio della Business Logic
        #
        ###########################################################
       
        numAvailabilityZoneDesiderated = desideratedAZ[2]

        if numAvailabilityZoneDesiderated > len(zonename):
            numAvailabilityZone = len(zonename)
        else:
            numAvailabilityZone = numAvailabilityZoneDesiderated 

        print subnets
        print zonename

        client = boto3.client('ec2')
        zonenameDesiderated = []
        subnetsAlreadyInUse = []
        for zname in client.describe_availability_zones()['AvailabilityZones']:
            zonenameDesiderated.append(zname['ZoneName'])
        subnetsMustToCreate = []
        for zname in zonenameDesiderated:
            i = 0
            for az in zonename:
                if(zname == az):
                    continue
                i= i + 1
            if(i == len(zonename)):
                subnetsMustToCreate.append(zname)

        print zonenameDesiderated
        print subnetsMustToCreate
        
        
        considerated = []
        i = 0
        for az in zonename:
            isConsiderated = False
            for c in considerated:
                if(az == c):
                    isConsiderated = True
            if isConsiderated==False:
                subnetsAlreadyInUse.append(subnets[i])
                considerated.append(az)
            i = i +1

        print subnetsAlreadyInUse
        print considerated

        cidrBlockAlreadyInUse = []
        for ipv4CidrBlock in client.describe_subnets()['Subnets']:
            cidrBlockAlreadyInUse.append(ipv4CidrBlock['CidrBlock'])

        print cidrBlockAlreadyInUse

        i = 2
        cidrBlock = '10.0.1.0/24'
        for az in subnetsMustToCreate:  
            restart = True
            while restart:
                 restart = False
                 for ipv4CidrBlock in cidrBlockAlreadyInUse:
                     if(ipv4CidrBlock == cidrBlock):
                         cidrBlock = '10.0.'+str(i) +'.0/24'
                         i = i +1
                         restart = True
                         break
            print cidrBlock
            cidrBlockAlreadyInUse.append(cidrBlock)

            subnet = vpc.create_subnet(
                   AvailabilityZone=az,
                   CidrBlock=cidrBlock
            );
            subnet.id
            subnetsAlreadyInUse.append(subnet.id)

        print subnetsAlreadyInUse
        subnets = subnetsAlreadyInUse
        zonename = zonenameDesiderated
        sgID = SecurityGroup.configParametersSpringBusinessLogic(ec2,vpcID)

        #MANCA UN AUTOSCALING FATTO BENE!!!!!!!!
        elbBLogic = LoadBalancerAutoscalingSpringBusinessLogic.configParametersBusinessLogic(regionNameVPC,vpcID,subnets,sgID,zonename, rdsAddress)
        print "\n[ELBv2] ------------ ",elbBLogic
        ###########################################################
        #
        #       Codice per il settaggio del Frontend
        #
        ###########################################################
        
        # deployFrontend(elbGO, elbBLogic)
        # print "\n[S3 AngularJS] ------------ "
