import boto3, time

###########################################################
#
#       RDS Settings
#
###########################################################
def createSubnet(az, vpcId):
    vpc = boto3.client('ec2')
    cidrBlock = '10.0.20.0/24'
    subnet = vpc.create_subnet(
        AvailabilityZone=az,
        CidrBlock=cidrBlock,
        VpcId=vpcId
    );
    print subnet
    return subnet['Subnet']['SubnetId']

def createRDS(sgID, vpcId):
    RDSInstance = boto3.client('rds')
    responseRDS = RDSInstance.create_db_instance(
        DBName='sdccdatalayer',
        DBInstanceIdentifier='sdccdatalayer',
        AllocatedStorage=20,
        DBInstanceClass='db.t2.micro',
        Engine='mysql',
        MasterUsername='fabrizio',
        MasterUserPassword='fabrizio',
        #DBSecurityGroups=[
        #'string',
        #],
        VpcSecurityGroupIds=[sgID],
        #AvailabilityZone='string',
        DBSubnetGroupName="default-"+vpcId,
        #PreferredMaintenanceWindow='string',
        #DBParameterGroupName='string',
        BackupRetentionPeriod=0,
        #PreferredBackupWindow='string',
        Port=3306,
        MultiAZ=False,
        EngineVersion='5.6.37',
        AutoMinorVersionUpgrade=False,
        LicenseModel='general-public-license',
        #Iops=1000,
        #OptionGroupName='string',
        #CharacterSetName='string',
        PubliclyAccessible=True,
        #Tags=[
         #   {
          #      'Key': 'string',
           #     'Value': 'string'
            #},
        #],
        #DBClusterIdentifier='string',
        StorageType='standard',
        #TdeCredentialArn='string',
        #TdeCredentialPassword='string',
        StorageEncrypted=False,
        #KmsKeyId='string',
        #Domain='string',
        CopyTagsToSnapshot=False,
        MonitoringInterval=0,
        #MonitoringRoleArn='string',
        #DomainIAMRoleName='string',
        #PromotionTier=0,
        #Timezone='string',
        EnableIAMDatabaseAuthentication=False,
        EnablePerformanceInsights=False,
        #PerformanceInsightsKMSKeyId='string'
    )
    print responseRDS['DBInstance']
    print "\n[RDS DATABASE] ------------- sdccdatalayer"
    return

def getEndPointRDS():
    client = boto3.client('rds')
    time.sleep(300) 
    response = client.describe_db_instances(
        DBInstanceIdentifier='sdccdatalayer')
    return response['DBInstances'][0]['Endpoint']['Address']
    