import boto3, json, os



def deployFrontend(elbGO, elbBLogic):
	os.system("aws s3 mb s3://fabrizioguglietti2.com")
	client = boto3.client('s3')
	# Create an S3 client
	s3 = boto3.client('s3')
	# Create the bucket policy
	bucket_policy = {
	    'Version': '2012-10-17',
	    'Statement': [{
		'Sid': 'PublicReadGetObject',
		'Effect': 'Allow',
		'Principal': '*',
		'Action': ['s3:GetObject'],
		'Resource': "arn:aws:s3:::fabrizioguglietti2.com/*"
	    }]
	}

	# Convert the policy to a JSON string
	bucket_policy = json.dumps(bucket_policy)

	# Set the new policy on the given bucket
	s3.put_bucket_policy(Bucket='fabrizioguglietti2.com', Policy=bucket_policy)

	#flag per dire che lo si sta usando per fare hosting di sito web statico, chiede di specificare che si chiama index.html
	goUrl = "\""+"http://"+elbGO+":8082/\""
	springUrl = "\""+"http://" + elbBLogic+":8080/\""
	os.system("echo '\n' >> ./frontend/config.js")
	os.system("echo "+ "'\t etcdUrl:" + goUrl + "', >> ./frontend/config.js")
	os.system("echo " + "'\t springUrl:"+ springUrl +"' >> ./frontend/config.js")
	os.system("echo '}' >> ./frontend/config.js")
	os.system("aws s3 sync ./frontend s3://fabrizioguglietti2.com")

	os.system("aws s3 website s3://fabrizioguglietti2.com/ --index-document index.html")
	return
