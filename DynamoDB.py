import boto3,shlex

###########################################################
#
#       DynamoDB Settings
#
###########################################################

def configParametersDynamoDBTables(regionNameVPC):

    configDDBFile = open(regionNameVPC+"/configDynamoDB.txt","r")    
    
    clientDynamoDB = boto3.client('dynamodb',region_name=regionNameVPC)

    tablesname = configDDBFile.readline().split(",")
                                             
    i = 0
    tablesnames = {}
    for t in tablesname:
        if i == 0:
            print ""
            i = i + 1
            continue    
        
        tName = shlex.split(t)[0]
        
        tableAttributes = tName.split(":")

        print tableAttributes
        if(len(tableAttributes)==2):
            tName = tableAttributes[0]
            print tName
            idField = tableAttributes[1]
            print idField

            clientDynamoDB.create_table(TableName=tName,        
                AttributeDefinitions=[  
                                        {   'AttributeName': idField,
                                            'AttributeType':'S'},
                                     ],

                KeySchema=[
                                        {
                                            "AttributeName": idField,
                                            "KeyType": "HASH"
                                        },
                                    ],
                
                ProvisionedThroughput={ 
                                    'WriteCapacityUnits': 5,
                                    'ReadCapacityUnits': 5
                                    }) 


        if(len(tableAttributes)==3):
            tName = tableAttributes[0]
            tablesnames [i-1] = tName
            print tName
            idField = tableAttributes[1]
            print idField
            idField2 = tableAttributes[2]
            print idField2
            clientDynamoDB.create_table(TableName=tName,        
                AttributeDefinitions=[  
                                        {   'AttributeName': idField,
                                            'AttributeType':'N'},
                                            {'AttributeName': idField2,
                                            'AttributeType':'N'},
                                     ],

                KeySchema=[
                                        {
                                            "AttributeName": idField,
                                            "KeyType": "HASH"
                                        },
                                        {
                                            "AttributeName": idField2,
                                            "KeyType": "RANGE"
                                        }
                                    ],
                
                ProvisionedThroughput={ 
                                    'WriteCapacityUnits': 5,
                                    'ReadCapacityUnits': 5
                                    }) 

        print "[DYNAMODB TABLE] ----------- ",tName
            
        i = i + 1

    return tablesnames
