import boto3, os, time

###########################################################
#
#	Deploy Lambda Functions e IoT Rules
#
###########################################################
    
def deployLambdaAndRules(regionNameVPC):
    region = regionNameVPC #"eu-west-1"
    os.system("cd lambda\n zip iot2DynamoDB.zip iot2DynamoDB.js")
    os.system("cd lambda\n zip iot2DynamoDBSignalation.zip iot2DynamoDBSignalation.js")

    #Create IAM client
    client = boto3.client('iam')
    response = client.create_role(
            Path='/',
            RoleName='lambdaFunction',
            AssumeRolePolicyDocument= '{"Version": "2012-10-17","Statement": [{"Effect": "Allow", "Principal":{"Service":"lambda.amazonaws.com"}, "Action": "sts:AssumeRole"}]}',
            Description='Admin role for lambda functions'
    )
    roleArn = response['Role']['Arn']
    print roleArn
    client.attach_role_policy(
        PolicyArn='arn:aws:iam::aws:policy/AmazonS3FullAccess',
        RoleName='lambdaFunction'
    )
    client.attach_role_policy(
        PolicyArn='arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess',
        RoleName='lambdaFunction'
    )
    time.sleep(200) 
    #roleArn = "arn:aws:iam::865044506028:role/lambdaFunction"
 #   print "aws lambda create-function \
 #   --region "+region+" \
 #   --function-name iot2DynamoDB2 \
 #   --zip-file "+ "fileb://"+os.path.abspath("./lambda/iot2DynamoDB.zip")+" \
 #   --role "+roleArn+" \
 #   --handler index.handler \
 #   --runtime nodejs6.10 \
 #   --timeout 3 \
 #   --memory-size 128"
    os.system("aws lambda create-function \
    --region "+region+" \
    --function-name iot2DynamoDB2 \
    --zip-file "+ "fileb://"+os.path.abspath("./lambda/iot2DynamoDB.zip")+" \
    --role "+roleArn+" \
    --handler index.handler \
    --runtime nodejs6.10 \
    --timeout 3 \
    --memory-size 128")

#    print "aws lambda create-function \
#    --region "+region+" \
#    --function-name iot2DynamoDBSignalation2 \
#    --zip-file "+ "fileb://"+os.path.abspath("./lambda/iot2DynamoDBSignalation.zip")+" \
#    --role "+roleArn+" \
#    --handler index.handler \
#    --runtime nodejs6.10 \
#    --timeout 3 \
#    --memory-size 128"

    os.system("aws lambda create-function \
    --region "+region+" \
    --function-name iot2DynamoDBSignalation2 \
    --zip-file "+ "fileb://"+os.path.abspath("./lambda/iot2DynamoDBSignalation.zip")+" \
    --role "+roleArn+" \
    --handler index.handler \
    --runtime nodejs6.10 \
    --timeout 3 \
    --memory-size 128")

    client = boto3.client('lambda')

    response = client.get_function(
        FunctionName='iot2DynamoDB2'
    )
    arnIot2DynamoDB = response['Configuration']['FunctionArn']

    response = client.get_function(
        FunctionName='iot2DynamoDBSignalation2'
    )
    arnIot2DynamoDBSignalation = response['Configuration']['FunctionArn']

    client = boto3.client('iot')
    response = client.create_topic_rule(
        ruleName='iotLambda2',
        topicRulePayload={
            'sql': "SELECT * FROM 'registration'",
            'description': 'Rule for registration',
            'actions': [
                {
                    'lambda': {
                        'functionArn': arnIot2DynamoDB
                    }
                },
            ],
            'ruleDisabled': False,
            'awsIotSqlVersion': '2016-03-23'
        }
    )

    response = client.create_topic_rule(
        ruleName='iotSendSignalation2',
        topicRulePayload={
            'sql': "SELECT * FROM 'iotSignalationTopic'",
            'description': 'Rule for iotSignalationTopic',
            'actions': [
                {
                    'lambda': {
                        'functionArn': arnIot2DynamoDBSignalation
                    }
                },
            ],
            'ruleDisabled': False,
            'awsIotSqlVersion': '2016-03-23'
        }
    )
    return