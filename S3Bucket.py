import boto3,shlex, os,time

###########################################################
#
#       S3 Bucket Settings
#
###########################################################

def configParametersS3Bucket():
                        #
                        #       Read Config Params from "configS3Bucket.txt"
                        #
    paramsConfigFiles3  = open("configS3Bucket.txt","r")

    bucketname          = paramsConfigFiles3.readline().split(",")[1]
    acl                 = paramsConfigFiles3.readline().split(",")[1]
    #path                = paramsConfigFiles3.readline().split(",")[1]
    #objectExec          = paramsConfigFiles3.readline().split(",")[1] 
    #aclObject           = paramsConfigFiles3.readline().split(",")[1]


    bucketname  = shlex.split(bucketname)[0]
    acl         = shlex.split(acl)[0]
    #path        = shlex.split(path)[0]
    #objectExec  = shlex.split(objectExec)[0]
    #aclObject   = shlex.split(aclObject)[0]


                        #
                        #       Create the S3 Bucket
                        #
    s3Bucket = boto3.client('s3')

    responseS3 = s3Bucket.create_bucket(
        ACL = acl,
        Bucket=bucketname,
        CreateBucketConfiguration={ 'LocationConstraint': 'eu-west-1' }
    )
    print responseS3
    #s3 = boto3.resource('s3')

                        #
                        #    set public permission 
                        #
    #s3.Object(bucketname, objectExec).put(Body=open(path, 'rb'))
    #obj = s3.Bucket(bucketname).Object(objectExec)
    #obj.Acl().put(ACL=aclObject)

    print "\n[S3 BUCKET] ---------------- ",bucketname

    os.system("aws cloudfront create-distribution \
                --origin-domain-name "+bucketname+".s3.amazonaws.com")
    
    return bucketname

def cloudFrontS3Bucket():
    paramsConfigFiles3  = open("configS3Bucket.txt","r")

    bucketname          = paramsConfigFiles3.readline().split(",")[1]
    acl                 = paramsConfigFiles3.readline().split(",")[1]
    #path                = paramsConfigFiles3.readline().split(",")[1]
    #objectExec          = paramsConfigFiles3.readline().split(",")[1] 
    #aclObject           = paramsConfigFiles3.readline().split(",")[1]

    time.sleep(10) 
    bucketname  = shlex.split(bucketname)[0]
    client = boto3.client('cloudfront')
    list = client.list_distributions()
    
    items = list['DistributionList']['Items']
    cloudfrontName = "default"
    for item in items:
        if(item['Origins']['Items'][0]['DomainName'] == bucketname+".s3.amazonaws.com"):
            cloudfrontName = item['DomainName']
    return cloudfrontName

