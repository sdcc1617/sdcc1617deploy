import boto3,shlex, time

###########################################################
#
#       Auto Scaling Settings
#
###########################################################

def confAutoScalingEtcdCluster(regionNameVPC,lbTg,subnets,zonename,sgID):

                        #
                        #       Read Config Params from "configAutoscaling.txt"
                        #

    # Create IAM client
    client = boto3.client('iam')

    response = client.create_instance_profile(
        InstanceProfileName='adminEtcdCluster',
        Path='/'
    )
    arn = response['InstanceProfile']['Arn']
    response = client.create_role(
        Path='/',
        RoleName='adminEtcdCluster',
        AssumeRolePolicyDocument= '{"Version": "2012-10-17","Statement": [{"Effect": "Allow", "Principal":{"Service":["ec2.amazonaws.com"]}, "Action": "sts:AssumeRole"}]}',
        Description='Admin role for Etcd Cluster'
    )

    client.attach_role_policy(
        PolicyArn='arn:aws:iam::aws:policy/AdministratorAccess',
        RoleName='adminEtcdCluster'
    )

    response = client.add_role_to_instance_profile(
        InstanceProfileName='adminEtcdCluster',
        RoleName='adminEtcdCluster'
    )
    time.sleep(30) 
    paramConfigFileAutoscaling  = open(regionNameVPC+"/configAutoscalingEtcd.txt","r")

    launchConfigName            = paramConfigFileAutoscaling.readline().split(",")[1]
    keyname                     = paramConfigFileAutoscaling.readline().split(",")[1]
    asgname                     = paramConfigFileAutoscaling.readline().split(",")[1]
    instanceSize                = paramConfigFileAutoscaling.readline().split(",")[1]
    imageEC2ID                  = paramConfigFileAutoscaling.readline().split(",")[1]
    maxSize                     = int(paramConfigFileAutoscaling.readline().split(",")[1])
    minSize                     = int(paramConfigFileAutoscaling.readline().split(",")[1])
    desiredCapacity             = int(paramConfigFileAutoscaling.readline().split(",")[1])
    defaultCooldown             = int(paramConfigFileAutoscaling.readline().split(",")[1])
    healthCheckGracePeriod      = int(paramConfigFileAutoscaling.readline().split(",")[1])

    launchConfigName            = shlex.split(launchConfigName)[0]
    keyname                     = shlex.split(keyname)[0]
    asgname                     = shlex.split(asgname)[0]
    instanceSize                = shlex.split(instanceSize)[0]
    imageEC2ID                  = shlex.split(imageEC2ID)[0]

    allSubnets = subnets[0]+","+subnets[1]+","+subnets[2]

    autoScaling = boto3.client('autoscaling',region_name=regionNameVPC)
    
                        ####    Launch Configuration
    responseAS = autoScaling.create_launch_configuration(
                            LaunchConfigurationName =  launchConfigName,
                            ImageId = imageEC2ID,
                            KeyName = keyname,
                            SecurityGroups= [sgID],
                            UserData= '#cloud-config\ncoreos:\n  etcd2:\n    advertise-client-urls: http://$private_ipv4:2379\n    initial-advertise-peer-urls: http://$private_ipv4:2380\n    listen-client-urls: http://0.0.0.0:2379\n    listen-peer-urls: http://$private_ipv4:2380\n  units:\n    - name: etcd-peers.service\n      command: start\n      content: |\n        [Unit]\n        Description=Write a file with the etcd peers that we should bootstrap to\n        After=docker.service\n        Requires=docker.service\n\n        [Service]\n        Type=oneshot\n        RemainAfterExit=yes\n        ExecStart=/usr/bin/docker pull monsantoco/etcd-aws-cluster:latest\n        ExecStart=/usr/bin/docker run --rm=true -v /etc/sysconfig/:/etc/sysconfig/ monsantoco/etcd-aws-cluster:latest\n    - name: etcd2.service\n      command: start\n      drop-ins:\n        - name: 30-etcd_peers.conf\n          content: |\n            [Unit]\n            After=etcd-peers.service\n            Requires=etcd-peers.service\n\n            [Service]\n            # Load the other hosts in the etcd leader autoscaling group from file\n            EnvironmentFile=/etc/sysconfig/etcd-peers\n    - name: fleet.service\n      command: start',      
                            InstanceType= instanceSize,
                            BlockDeviceMappings= [
                                {
                                    'DeviceName': '/dev/xvda',
                                    'Ebs': {
                                        'VolumeType': 'gp2',
                                        'VolumeSize': 8,
                                        'DeleteOnTermination': True
                                            }
                                }],
                            InstanceMonitoring= {'Enabled': True },
                            EbsOptimized= False,
                            IamInstanceProfile= arn,
                            AssociatePublicIpAddress=True
                            )
    print "[AUTOSCALING CONF. NAME] --- ",launchConfigName

                        ####    Create Auto-Scaling Group
    responseASG = autoScaling.create_auto_scaling_group(
                            AutoScalingGroupName= asgname,
                            LaunchConfigurationName= launchConfigName,
                            MaxSize= maxSize,
                            MinSize= minSize,
                            DesiredCapacity= desiredCapacity,
                            DefaultCooldown= defaultCooldown,
                            AvailabilityZones= [zonename[0],zonename[1],zonename[2]],
                            TargetGroupARNs= [lbTg],
                            HealthCheckType= 'EC2',
                            HealthCheckGracePeriod= healthCheckGracePeriod,
                            VPCZoneIdentifier= allSubnets,
                            TerminationPolicies= ['Default'],
                            NewInstancesProtectedFromScaleIn= True
                           )

    print "[AUTOSCALING GROUP NAME] --- ",asgname
    return


###########################################################
#
#       Load Balancer Settings
#
###########################################################

def configParametersEtcdCluster(regionNameVPC,vpcID,subnets,sgID,zonename):

                        #
                        #       read Config Params from "configLoadBalancer.txt"
                        #

    paramConfigFileLB   = open(regionNameVPC+"/configLoadBalancerEtcdCluster.txt","r")

    nameLB              = paramConfigFileLB.readline().split(",")[1]
    targetGroupName     = paramConfigFileLB.readline().split(",")[1]
    protocol            = paramConfigFileLB.readline().split(",")[1]
    port                = int(paramConfigFileLB.readline().split(",")[1])
    healthCheckTimeSeconds      = int(paramConfigFileLB.readline().split(",")[1])
    healthyThresholdCount       = int(paramConfigFileLB.readline().split(",")[1])
    unhealthyThresholdCount     = int(paramConfigFileLB.readline().split(",")[1])
    healthCheckIntervalSeconds  = int(paramConfigFileLB.readline().split(",")[1])

    nameLB              = shlex.split(nameLB)[0]
    targetGroupName     = shlex.split(targetGroupName)[0]
    protocol            = shlex.split(protocol)[0]

    loadBalancer = boto3.client('elbv2',region_name=regionNameVPC)
    

    responseLB = loadBalancer.create_load_balancer(
                    Name= nameLB,
                    Subnets= [subnets[0], subnets[1], subnets[2]],
                    SecurityGroups= [sgID],
                    Scheme= 'internet-facing',
                    Type= 'application', 
                    )

    lbArn = responseLB['LoadBalancers'][0]['LoadBalancerArn']
    elbEtcd = responseLB['LoadBalancers'][0]['DNSName']

                        ####    Target Group
    responseTG = loadBalancer.create_target_group(
                    Name = targetGroupName,
                    Protocol= protocol,
                    Port= port,
                    VpcId= vpcID,
                    HealthCheckPort= 'traffic-port',
                    HealthCheckTimeoutSeconds= healthCheckTimeSeconds,
                    HealthCheckProtocol= protocol,
                    HealthyThresholdCount= healthyThresholdCount,
                    UnhealthyThresholdCount= unhealthyThresholdCount,
                    HealthCheckPath= '/v2/keys?recursive=true',
                    HealthCheckIntervalSeconds= healthCheckIntervalSeconds,
                    Matcher= {  'HttpCode': '200' },
                )

    lbTg = responseTG['TargetGroups'][0]['TargetGroupArn']


                    ### Attach a Listener: Assign the port number 8000
    responseLB = loadBalancer.create_listener(
                    LoadBalancerArn = lbArn,
                    Protocol=protocol,
                    Port=port,
                    DefaultActions=[
                    {
                        'Type':'forward',
                        'TargetGroupArn': lbTg
                    },])
    responseLB = loadBalancer.create_listener(
                    LoadBalancerArn = lbArn,
                    Protocol=protocol,
                    Port=80,
                    DefaultActions=[
                    {
                        'Type':'forward',
                        'TargetGroupArn': lbTg
                    },])

    print "\n[LOAD BALANCER] ------------ ",nameLB
    print "[TARGET GROUP] ------------- ",targetGroupName
    
    confAutoScalingEtcdCluster(regionNameVPC,lbTg,subnets,zonename,sgID)

    return elbEtcd